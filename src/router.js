import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Dashboard.vue'),
      meta: {requiresAuth: true},
      beforeEnter (to, from, next) {
      if (!loggedIn()) {
      return next({
      name: 'login'
      })
      }
      next()
      }
    },
    {
      path: '/register',
      name: 'register',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/RegisterUser.vue')
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/LoginUser.vue')
    }

  ]
})
//  router.beforeEach((to, from, next) => {
//    const loggedIn = localStorage.getItem('user')
//     if(to.matched.some(record => record.meta.requiresAuth)){
//       if (loggedIn) {
//         next('/')
//           return
//       }
//         next()
//     }
//     next()
//  })
// export default router
